package net.islandearth.dregora.trade;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Villager;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import net.islandearth.dregora.trade.api.MerchantsAPI;
import net.islandearth.dregora.trade.commands.AcceptCommand;
import net.islandearth.dregora.trade.commands.DeclineCommand;
import net.islandearth.dregora.trade.commands.MerchantCommand;
import net.islandearth.dregora.trade.commands.TabComplete;
import net.islandearth.dregora.trade.listeners.EntityListener;
import net.islandearth.dregora.trade.listeners.InventoryListener;
import net.islandearth.dregora.trade.listeners.MerchantCreateListener;
import net.islandearth.dregora.trade.listeners.MerchantTradeListener;
import net.islandearth.dregora.trade.listeners.PlayerListener;
import net.islandearth.dregora.trade.listeners.VillagerListener;
import net.islandearth.dregora.trade.merchant.AdminMerchant;
import net.islandearth.dregora.trade.merchant.HireMerchant;
import net.islandearth.dregora.trade.merchant.IMerchant;
import net.islandearth.dregora.trade.merchant.MerchantFactory;
import net.islandearth.dregora.trade.merchant.MerchantInfo;
import net.islandearth.dregora.trade.merchant.NormalMerchant;
import net.islandearth.dregora.trade.metrics.Metrics;
import net.islandearth.dregora.trade.tasks.ExpirationTask;
import net.islandearth.dregora.trade.utils.MerchantUtils;
import net.islandearth.dregora.trade.utils.TradeCache;
import net.islandearth.dregora.trade.version.VersionChecker;
import net.islandearth.dregora.trade.version.VersionChecker.Version;
import net.islandearth.languagy.language.Language;
import net.islandearth.languagy.language.LanguagyImplementation;
import net.islandearth.languagy.language.LanguagyPluginHook;
import net.islandearth.languagy.language.Translator;
import net.milkbowl.vault.economy.Economy;

public class Merchants extends JavaPlugin implements LanguagyPluginHook {
	
	// Cache
	@Getter
	private TradeCache tradeCache;
	
	// Languagy translator
	@LanguagyImplementation(fallbackFile = "plugins/Merchants/lang/en_gb.yml")
	@Getter
	private Translator translator;
	
	// Merchant factory
	@Getter
	private MerchantFactory merchantFactory;
	
	// Vault Economy
	@Getter
	private Economy economy;
	
	@Getter
	private VersionChecker versionChecker;
	
	public static String USER = "%%__USER__%%";
	
	@Override
	public void onEnable() {
		
		// Check requirements
		if (Bukkit.getPluginManager().getPlugin("Languagy") == null) {
			this.getLogger().severe("Languagy is not installed! Merchants is unable to start.");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		
		if (!setupEconomy()) {
            this.getLogger().severe("Vault is either not installed or there is no economy!");
        }
		
		this.versionChecker = new VersionChecker();
		this.checkVersion();
		
		// Register listeners, create API, files, etc.
		createFiles();
		registerCommands();
		registerListeners();
		startTasks();
		
		MerchantsAPI.setPlugin(this);
		this.tradeCache = new TradeCache();
		this.merchantFactory = new MerchantFactory();
		
		additionalConfig();
		
		// Load bought items that need to be processed
		for (File file : new File(getDataFolder() + "/data/merchants/").listFiles()) {
			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			if (config.getInt("bought") != 0) {
				Map<UUID, String> entityMap = new HashMap<>();
				entityMap.put(UUID.fromString(file.getName().replace(".yml", "")), config.getString("bought"));
				tradeCache.getBought().put(UUID.fromString(config.getString("owner")), entityMap);
			}
		}
		
		new Metrics(this);
	}
	
	@Override
	public void onDisable() {
		//TODO reload support
	}

	/**
	 * Creates required files
	 */
	private void createFiles() {
		File merchants = new File(getDataFolder() + "/data/merchants/");
		if (!merchants.exists()) {
			merchants.mkdirs();
		}
		
		File merchants2 = new File(getDataFolder() + "/merchants/");
		if (!merchants2.exists()) {
			merchants2.mkdirs();
		}
		
		File players = new File(getDataFolder() + "/players/");
		if (!players.exists()) {
			players.mkdirs();
		}
		
		File lang = new File(getDataFolder() + "/lang/");
		if (!lang.exists()) lang.mkdirs();
		
		for (Language language : Language.values()) {
			File file = new File(getDataFolder() + "/lang/" + language.getCode() + ".yml");
			if (!file.exists()) {
			   try {
			       file.createNewFile();
			   } catch (IOException e1) {
			       e1.printStackTrace();
			   }
			}
			
			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			config.options().copyDefaults(true);
			config.addDefault("not_owner", "&cYou are not the owner of this merchant, or it does not have one!");
			config.addDefault("already_trader", "&cThat villager is already a trader! Please contact an admin if you believe this is a mistake.");
			config.addDefault("not_found", "&cCould not find villager! Please contact an admin if this issue persists.");
			config.addDefault("no_active_deal", "&cYou do not have an active trade deal.");
			config.addDefault("trade_offer", "\"You have traded with me for a long time. I'm willing to become your trader, I believe it will benefit us both!\"");
			config.addDefault("trade_finished", "\"Shift + Right Click me to add a trade. I look forward to further deals between us!\"");
			config.addDefault("accept", "Accept the offer");
			config.addDefault("decline", "Decline the offer");
			config.addDefault("trade_saved", "&aTrade saved.");
			config.addDefault("incorrect_item", "&cThis merchants current trade does not require that item.");
			config.addDefault("not_enough", "&cYou need more of that item to stock this merchant.");
			config.addDefault("successfully_added", "&aYou have successfully stocked this merchant!");
			config.addDefault("sold_item", "&e{0} &ahas bought &f{1} &afrom one of your stores.");
			config.addDefault("offline_sold_item", "&aWhilst you were offline, you sold &f{1} &at one of your stores.");
			config.addDefault("max_stock", "&cYou have reached the maximum stock for this store.");
			config.addDefault("max_merchants", "&cYou have reached the maximum amount of merchants. Donate for more at our store.");
			config.addDefault("no_permission", "&cYou do not have permission to execute this command.");
			config.addDefault("admin_merchant_created", "&aAdmin merchant created. Shift + Right Click to edit.");
			config.addDefault("admin_cannot_stock", "&cThis is an admin merchant. It cannot be stocked.");
			config.addDefault("hire_merchant_created", "&aHire merchant created. Player's with the permission Merchant.hire can hire this merchant.");
			config.addDefault("hire", "\"If you want, you can hire me! I only cost {xp}xp and £{cost}!\"");
			config.addDefault("expired", "&cOne of your hired merchants has expired.");
			config.addDefault("cannot_afford", "&cYou are unable to hire this merchant as you do not have the required balance.");
			config.addDefault("hired", "&aYou have hired this merchant for {0} week.");
			config.addDefault("trade_declined", "&cYou have cancelled the trade deal.");
			config.addDefault("not_donator", "&cIn order to do this, you need to purchase a rank on our store!");
			try {
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		getConfig().options().copyDefaults(true);
		getConfig().addDefault("Minimum Trades", 2);
		getConfig().addDefault("Prevent Merchant Damage", true);
		getConfig().addDefault("Consume Rename", true);
		getConfig().addDefault("Sql.enabled", false);
		//TODO merchant upgrades
		//TODO 1.14 discount support
		saveConfig();
	}
	
	/**
	 * Registers commands
	 */
	private void registerCommands() {
		getCommand("createtradedeal").setExecutor(new AcceptCommand(this));
		getCommand("merchant").setExecutor(new MerchantCommand(this));
		this.getCommand("merchant").setTabCompleter(new TabComplete().withSuggestion("merchant", Arrays.asList("about", "permissions", "types", "create"))
				.withSuggestion("create", Arrays.asList("admin", "hire"))
				.withSuggestion("admin", "[<args>]")
				.withSuggestion("hire", "[<args>]"));
		getCommand("declinetradedeal").setExecutor(new DeclineCommand(this));
	}
	
	/**
	 * Registers plugin listeners
	 */
	private void registerListeners() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new InventoryListener(this), this);
		pm.registerEvents(new MerchantTradeListener(this), this);
		pm.registerEvents(new EntityListener(this), this);
		pm.registerEvents(new PlayerListener(this), this);
		pm.registerEvents(new MerchantCreateListener(this), this);
		pm.registerEvents(new VillagerListener(this), this);
	}
	
	/**
	 * Start plugin tasks
	 */
	private void startTasks() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new ExpirationTask(this), 0L, 40L);
	}
	
	/**
	 * Additional merchant config values
	 */
	private void additionalConfig() {
		MerchantInfo donor = getMerchantFactory().getByName("donator");
		if (!donor.getConfig().isSet("Glow Item")) donor.getConfig().set("Glow Item", Material.BLAZE_ROD.toString());
		if (!donor.getConfig().isSet("Slots")) donor.getConfig().set("Slots", 5);
		donor.saveConfig();
		
		MerchantInfo hire = getMerchantFactory().getByName("hire");
		if (!hire.getConfig().isSet("weeks")) hire.getConfig().set("weeks", 1);
		if (!hire.getConfig().isSet("cost")) hire.getConfig().set("cost", 150);
		if (!hire.getConfig().isSet("xpcost")) hire.getConfig().set("xpcost", 200);
		hire.saveConfig();
	}
	
	/**
	 * Sets up vault economy
	 * @return true if successful
	 */
	private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        
        economy = rsp.getProvider();
        return economy != null;
    }
	
	/**
	 * Don't use for api!
	 * Loads the specified villager into the {@link TradeCache}.
	 * @param entity
	 */
	public void load(Villager entity) {
		IMerchant merchant = new NormalMerchant(entity);
		if (MerchantUtils.isAdmin(entity)) merchant = new AdminMerchant(entity);
		else if (MerchantUtils.isHire(entity)) merchant = new HireMerchant(entity);
		tradeCache.getMerchants().put(entity.getUniqueId(), merchant);
	}

	@Override
	public void onLanguagyHook() {
		translator.setDisplay(Material.HAY_BLOCK);
	}
	
	/**
	 * Check server compatibility
	 * @return true if server is using a compatible version
	 */
	private boolean checkVersion() {
		this.versionChecker = new VersionChecker();
		List<String> supported = new ArrayList<>();
		for (Version version : Version.values()) {
			if (version != Version.UNSUPPORTED) {
				supported.add(version.getId());
			}
		}
		
		if (!versionChecker.checkVersion()) {
			this.getLogger().info(" ");
			this.getLogger().info(ChatColor.RED + "You are using an unsupported version!");
			this.getLogger().info(ChatColor.RED + "Your current version is: " + versionChecker.getCurrentVersion().getId());
			this.getLogger().info(ChatColor.RED + "The latest version is: " + versionChecker.getLatestVersion().getId());
			this.getLogger().info(ChatColor.GREEN + "This plugin supports: " + StringUtils.join(supported, ','));
			this.getLogger().info(" ");
			return false;
		}
		
		this.getLogger().info(" ");
		this.getLogger().info(ChatColor.GREEN + "You are running version " + versionChecker.getCurrentVersion().getId() + ".");
		this.getLogger().info(" ");
		return true;
	}
	
	/**
	 * Get the current save method the server is using
	 * @return type
	 */
	public SaveType getSaveType() {
		if (getConfig().getBoolean("Sql.enabled")) return SaveType.SQL;
		return SaveType.FILE;
	}
	
	public enum SaveType {
		SQL,
		FILE;
	}
}
