package net.islandearth.dregora.trade.sql;

import java.util.UUID;
import java.util.function.Consumer;

public interface SQLManager {
	
	public void getString(String column, UUID uuid, Consumer<String> data);

}
