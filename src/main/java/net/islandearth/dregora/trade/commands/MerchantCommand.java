package net.islandearth.dregora.trade.commands;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

import lombok.AllArgsConstructor;
import net.islandearth.dregora.trade.Merchants;
import net.islandearth.dregora.trade.events.MerchantCreateEvent;
import net.islandearth.dregora.trade.merchant.AdminMerchant;
import net.islandearth.dregora.trade.merchant.HireMerchant;
import net.islandearth.dregora.trade.merchant.MerchantInfo;
import net.islandearth.dregora.trade.permissions.MerchantsPermissions;

@AllArgsConstructor
public class MerchantCommand implements CommandExecutor {

	private Merchants plugin;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			switch (args.length) {
				case 0:
					sendHelp(player);
					break;
				case 1: {
					switch (args[0].toLowerCase()) {
						case "types":
							player.sendMessage(ChatColor.GREEN + "Registered merchant types: ");
							player.sendMessage(ChatColor.LIGHT_PURPLE + "[Name] : [Description]");
							for (MerchantInfo merchant : plugin.getMerchantFactory().getMerchants()) {
								player.sendMessage(merchant.getMerchantName() + " : " + merchant.getDescription());
							}
							break;
						case "permissions":
							player.sendMessage(ChatColor.GREEN + "Permissions: ");
							player.sendMessage(ChatColor.LIGHT_PURPLE + "[Description] : [Permission]");
							for (MerchantsPermissions permission : MerchantsPermissions.values()) {
								player.sendMessage(permission.toString() + " : " + permission.getPermission());
							}
							
							for (MerchantInfo merchant : plugin.getMerchantFactory().getMerchants()) {
								player.sendMessage(merchant.getMerchantName() + " : " + merchant.getPermission());
							}
							break;
						case "about":
							player.sendMessage(ChatColor.GREEN + "This plugin is registered to: https://www.spigotmc.org/members/" + Merchants.USER + "/");
							player.sendMessage(ChatColor.RED + "If you believe this plugin is unregistered and being used commercially, please report violations to sam@islandearth.net or https://gitlab.com/dregora/dregora-trade/issues.");
							break;
					}
					break;
				}
				
				case 3: {
					switch (args[0].toLowerCase()) {
						case "create": {
							switch (args[1].toLowerCase()) {
								case "admin": {
									if (player.hasPermission(plugin.getMerchantFactory().getByName("admin").getPermission())) {
										LivingEntity entity = (LivingEntity) player.getWorld().spawnEntity(player.getLocation(), EntityType.VILLAGER);
										entity.setAI(false);
										entity.setCustomName(ChatColor.translateAlternateColorCodes('&', args[2].replace("_", " ")));
										entity.setCustomNameVisible(true);
										File file = new File(plugin.getDataFolder() + "/data/merchants/" + entity.getUniqueId() + ".yml");
										if (!file.exists()) {
											try {
												file.createNewFile();
												FileConfiguration config = YamlConfiguration.loadConfiguration(file);
												config.options().copyDefaults(true);
												config.addDefault("admin", true);
												config.save(file);
											} catch (IOException e) {
												e.printStackTrace();
											}
										}
										
										Villager villager = (Villager) entity;
										AdminMerchant merchant = new AdminMerchant(villager);
										merchant.setMerchantTrade(player, 0);
										for (int i = 0; i < villager.getRecipes().size(); i++) {
											merchant.setMerchantTrade(player, i);
										}
										player.sendMessage(plugin.getTranslator().getTranslationFor(player, "admin_merchant_created"));
										Bukkit.getPluginManager().callEvent(new MerchantCreateEvent(player, merchant));
									} else {
										player.sendMessage(plugin.getTranslator().getTranslationFor(player, "no_permission"));
									}
									break;
								}
								
								case "hire": {
									if (plugin.getEconomy() != null) {
										if (player.hasPermission(plugin.getMerchantFactory().getByName("hire").getPermission())) {
											LivingEntity entity = (LivingEntity) player.getWorld().spawnEntity(player.getLocation(), EntityType.VILLAGER);
											entity.setAI(false);
											entity.setCustomName(ChatColor.translateAlternateColorCodes('&', args[2].replace("_", " ")));
											entity.setCustomNameVisible(true);
											File file = new File(plugin.getDataFolder() + "/data/merchants/" + entity.getUniqueId() + ".yml");
											if (!file.exists()) {
												try {
													file.createNewFile();
													FileConfiguration config = YamlConfiguration.loadConfiguration(file);
													config.options().copyDefaults(true);
													config.addDefault("hire", true);
													config.addDefault("owner", "none");
													config.save(file);
												} catch (IOException e) {
													e.printStackTrace();
												}
											}
											
											Villager villager = (Villager) entity;
											HireMerchant hire = new HireMerchant(villager);
											for (int i = 0; i < villager.getRecipes().size(); i++) {
												hire.setMerchantTrade(player, i);
											}
											player.sendMessage(plugin.getTranslator().getTranslationFor(player, "hire_merchant_created"));
											Bukkit.getPluginManager().callEvent(new MerchantCreateEvent(player, hire));
										} else {
											player.sendMessage(plugin.getTranslator().getTranslationFor(player, "no_permission"));
										}
									} else {
										player.sendMessage(ChatColor.RED + "Vault is not installed!");
									}
									break;
								}
								
								default:
									player.sendMessage(ChatColor.RED + "Unknown merchant type!");
									break;
							}
							break;
						}
					}
				}
			}
		} else {
			sender.sendMessage(ChatColor.RED + "You need to be a player to execute this command.");
		}
		return true;
	}
	
	private void sendHelp(Player player) {
		player.sendMessage("");
		player.sendMessage(ChatColor.GREEN + "- - - - - - - - - - - - -" + ChatColor.BLUE + " Merchants " + ChatColor.GREEN + "- - - - - - - - - - - - -");
		player.sendMessage(ChatColor.YELLOW + "/merchant create (type) (name) " + ChatColor.WHITE + "creates an admin merchant with the specified name");
		player.sendMessage(ChatColor.YELLOW + "/merchant types " + ChatColor.WHITE + "display all registered merchant types");
		player.sendMessage(ChatColor.YELLOW + "/merchant about " + ChatColor.WHITE + "registration information");
		player.sendMessage(ChatColor.YELLOW + "/merchant permissions " + ChatColor.WHITE + "list permissions");
		player.sendMessage(ChatColor.YELLOW + "© 2019 IslandEarth. All Rights reserved.");
		player.sendMessage("");
	}
}
