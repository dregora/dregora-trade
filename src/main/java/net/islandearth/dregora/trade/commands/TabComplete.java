package net.islandearth.dregora.trade.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import net.islandearth.dregora.trade.permissions.MerchantsPermissions;

public class TabComplete implements TabCompleter {
	
	private Map<String, List<String>> completions = new HashMap<>();
	
	private Map<String, MerchantsPermissions> permissions = new HashMap<>();

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		switch (args.length) {
			case 1: {
				if (!completions.containsKey(cmd.getName().toLowerCase())) return null;
				List<String> results = new ArrayList<String>();
				
				for (String string : completions.get(cmd.getName().toLowerCase())) {
					Pattern pattern = Pattern.compile(args[0].toLowerCase());
					Matcher matcher = pattern.matcher(string.toLowerCase());
					
					while (matcher.find()) {
						if (hasPermissionForArg(string.toLowerCase(), sender)) results.add(string);
					}
				}
				
				if (!results.isEmpty()) return results;
				return null;
			}
			
			default: {
				if (!completions.containsKey(args[args.length - 2].toLowerCase())) return null;
				List<String> results = new ArrayList<String>();
				
				for (String string : completions.get(args[args.length - 2].toLowerCase())) {
					Pattern pattern = Pattern.compile(args[args.length - 1].toLowerCase());
					Matcher matcher = pattern.matcher(string.toLowerCase());
					
					while (matcher.find()) {
						if (hasPermissionForArg(string, sender)) results.add(string);
					}
				}
				
				if (!results.isEmpty()) return results;
				return null;
			}
		}
	}
	
	public TabComplete withSuggestion(String rootArg, String suggestion) {
		if (!completions.containsKey(rootArg)) {
			completions.put(rootArg, Arrays.asList(suggestion));
		} else {
			List<String> current = new ArrayList<>(completions.get(rootArg));
			current.add(suggestion);
			completions.replace(rootArg, current);
		}
		return this;
	}
	
	public TabComplete withSuggestion(String rootArg, List<String> suggestions) {
		for (String suggestion : suggestions) {
			this.withSuggestion(rootArg, suggestion);
		}
		return this;
	}
	
	public TabComplete requirePermission(String rootArg, MerchantsPermissions permission) {
		if (!permissions.containsKey(rootArg)) {
			permissions.put(rootArg, permission);
		} else {
			permissions.replace(rootArg, permission);
		}
		return this;
	}
	
	private boolean hasPermissionForArg(String rootArg, CommandSender sender) {
		if (!permissions.containsKey(rootArg)) return true;
		return sender.hasPermission(permissions.get(rootArg).getPermission());
	}
}
