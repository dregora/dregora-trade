package net.islandearth.dregora.trade.commands;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantRecipe;
import org.bukkit.permissions.PermissionAttachmentInfo;

import lombok.AllArgsConstructor;
import net.islandearth.dregora.trade.Merchants;
import net.islandearth.dregora.trade.events.MerchantCreateEvent;
import net.islandearth.dregora.trade.merchant.DonatorMerchant;
import net.islandearth.dregora.trade.merchant.HireMerchant;
import net.islandearth.dregora.trade.merchant.IMerchant;
import net.islandearth.dregora.trade.merchant.NormalMerchant;
import net.islandearth.dregora.trade.utils.MerchantUtils;
import net.milkbowl.vault.economy.EconomyResponse;

@AllArgsConstructor
public class AcceptCommand implements CommandExecutor {

	private Merchants plugin;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (plugin.getTradeCache().getTrading().containsKey(player.getUniqueId())) {
				Villager target = plugin.getTradeCache().getTradingEntities().get(player.getUniqueId()).getMerchant();
				if (target != null) {
					File pf = new File(plugin.getDataFolder() + "/players/" + player.getUniqueId() + ".yml");
					FileConfiguration pfc = YamlConfiguration.loadConfiguration(pf);
					
					// Check whether player has too many merchants
					if (pfc.getInt("Merchants") >= getMaxPermission(player)) {
						player.sendMessage(plugin.getTranslator().getTranslationFor(player, "max_merchants"));
						return true;
					}
					
					// Handle HireMerchant
					if (plugin.getTradeCache().getTradingEntities().get(player.getUniqueId()) instanceof HireMerchant) {
						int cost = plugin.getMerchantFactory().getByName("hire").getConfig().getInt("cost");
						int exp = plugin.getMerchantFactory().getByName("hire").getConfig().getInt("xpcost");
						
						if (plugin.getEconomy().getBalance(player) < cost || MerchantUtils.getPlayerExp(player) < exp) {
							player.sendMessage(plugin.getTranslator().getTranslationFor(player, "cannot_afford"));
						} else {
							EconomyResponse er = plugin.getEconomy().withdrawPlayer(player, cost);
							if (er.transactionSuccess()) {
								MerchantUtils.changePlayerExp(player, MerchantUtils.getPlayerExp(player) - exp);
								player.sendMessage(plugin.getTranslator().getTranslationFor(player, "hired").replace("{0}", "" + plugin.getMerchantFactory().getByName("hire").getConfig().getInt("weeks")));
								LocalDateTime now = LocalDateTime.now().plusWeeks(plugin.getMerchantFactory().getByName("hire").getConfig().getInt("weeks"));
								File file = new File(plugin.getDataFolder() + "/data/merchants/" + target.getUniqueId() + ".yml");
								FileConfiguration config = YamlConfiguration.loadConfiguration(file);
								config.set("owner", player.getUniqueId().toString());
								config.set("level", 1);
								config.set("expiration", DateTimeFormatter.ISO_DATE_TIME.format(now));
								try {
									config.save(file);
								} catch (IOException e) {
									e.printStackTrace();
								}
								
								// Reset recipes
								if (target.getRecipes().size() > plugin.getTradeCache().getMerchants().get(target.getUniqueId()).getMerchant().getVillagerLevel()) {
									
									List<MerchantRecipe> recipes = new ArrayList<>();
									for (int i = 0; i < plugin.getTradeCache().getMerchants().get(target.getUniqueId()).getMerchant().getVillagerLevel(); i++) {
										recipes.add(new MerchantRecipe(new ItemStack(Material.AIR), 0));
									}
									target.setRecipes(recipes);
								}
								
								plugin.getTradeCache().getTradingEntities().remove(player.getUniqueId());
								plugin.getTradeCache().getTrading().remove(player.getUniqueId());
							} else {
								player.sendMessage(ChatColor.RED + "An error occured within Vault. Please contact an administrator.");
							}
						}
						
						return true;
					}
					
					File file = new File(plugin.getDataFolder() + "/data/merchants/" + target.getUniqueId() + ".yml");
					if (!file.exists()) {
						try {
							file.createNewFile();
							FileConfiguration config = YamlConfiguration.loadConfiguration(file);
							config.set("owner", player.getUniqueId().toString());
							config.set("level", 1);
							config.save(file);
							plugin.getTradeCache().getTrading().remove(player.getUniqueId());
							plugin.getTradeCache().getTradingEntities().remove(player.getUniqueId());
							player.sendMessage(plugin.getTranslator().getTranslationFor(player, "trade_finished"));
							player.updateInventory();
							IMerchant merchant = new NormalMerchant(target);
							if (player.hasPermission(plugin.getMerchantFactory().getByName("donator").getPermission())) merchant = new DonatorMerchant(target);
							for (int i = 0; i < target.getRecipes().size(); i++) {
								merchant.setMerchantTrade(player, i);
							}
							Bukkit.getPluginManager().callEvent(new MerchantCreateEvent(player, merchant));
						} catch (IOException e) {
							e.printStackTrace();
						}
					} else {
						player.sendMessage(plugin.getTranslator().getTranslationFor(player, "already_trader"));
					}
				} else {
					player.sendMessage(plugin.getTranslator().getTranslationFor(player, "not_found"));
				}
			} else {
				player.sendMessage(plugin.getTranslator().getTranslationFor(player, "no_active_deal"));
			}
		} else {
			sender.sendMessage(ChatColor.RED + "You need to be a player to execute this command.");
		}
		return true;
	}
	
	private int getMaxPermission(Player player) {
		int max = 1;
		for (PermissionAttachmentInfo perm : player.getEffectivePermissions()) {
			if (perm.getPermission().startsWith("merchant.limit.")) {
				try {
					int newMax = Integer.parseInt(perm.getPermission().replace("merchant.limit.", ""));
					if (newMax > max) {
						max = newMax;
					}
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
		}
		return max;
	}
}
