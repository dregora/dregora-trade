package net.islandearth.dregora.trade.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

import lombok.AllArgsConstructor;
import net.islandearth.dregora.trade.Merchants;

@AllArgsConstructor
public class DeclineCommand implements CommandExecutor {

	private Merchants plugin;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (plugin.getTradeCache().getTrading().containsKey(player.getUniqueId())) {
				Villager target = plugin.getTradeCache().getTradingEntities().get(player.getUniqueId()).getMerchant();
				if (target != null) {
					player.sendMessage(plugin.getTranslator().getTranslationFor(player, "trade_declined"));
					plugin.getTradeCache().getTradingEntities().remove(player.getUniqueId());
					plugin.getTradeCache().getTrading().remove(player.getUniqueId());
				} else {
					player.sendMessage(plugin.getTranslator().getTranslationFor(player, "not_found"));
				}
			} else {
				player.sendMessage(plugin.getTranslator().getTranslationFor(player, "no_active_deal"));
			}
		} else {
			sender.sendMessage(ChatColor.RED + "You need to be a player to execute this command.");
		}
		return true;
	}
}
