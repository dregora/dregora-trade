package net.islandearth.dregora.trade.api;

import net.islandearth.dregora.trade.Merchants;
import net.islandearth.dregora.trade.merchant.MerchantFactory;

public class MerchantsAPI {
	
	private static Merchants plugin;
	
	private MerchantsAPI() {}

	public static MerchantFactory getMerchantFactory() {
		return getPlugin().getMerchantFactory();
	}
	
	/**
	 * Sets a new {@link Merchants} instance for the API.
	 * @param instance
	 */
	public static void setPlugin(Merchants instance) {
		if (getPlugin() != null) {
			throw new IllegalArgumentException("API: Plugin instance is already set!");
		}
		
		plugin = instance;
	}
	
	/**
	 * @return {@link Merchants} instance
	 */
	public static Merchants getPlugin() {
		return plugin;
	}
}
