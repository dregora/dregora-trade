package net.islandearth.dregora.trade.events;

import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import lombok.Getter;

public class MerchantTradeEvent extends Event {
	
	public static HandlerList handlers = new HandlerList();
	
	@Getter
	private Player player;
	
	@Getter
	private Villager merchant;
	
	@Getter
	private int index;
	
	public MerchantTradeEvent(Player player, Villager merchant, int index) {
		this.player = player;
		this.merchant = merchant;
		this.index = index;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
