package net.islandearth.dregora.trade.events;

import org.bukkit.entity.Villager;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import lombok.Getter;

public class MerchantRemoveEvent extends Event {
	
	public static HandlerList handlers = new HandlerList();
	
	@Getter
	private Villager merchant;
	
	public MerchantRemoveEvent(Villager merchant) {
		this.merchant = merchant;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
