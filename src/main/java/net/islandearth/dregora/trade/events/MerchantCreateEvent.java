package net.islandearth.dregora.trade.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import lombok.Getter;
import net.islandearth.dregora.trade.merchant.IMerchant;

public class MerchantCreateEvent extends Event {
	
	public static HandlerList handlers = new HandlerList();
	
	@Getter
	private Player player;
	
	@Getter
	private IMerchant merchant;
	
	public MerchantCreateEvent(Player player, IMerchant merchant) {
		this.player = player;
		this.merchant = merchant;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
