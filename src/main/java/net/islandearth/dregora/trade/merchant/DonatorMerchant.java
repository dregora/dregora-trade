package net.islandearth.dregora.trade.merchant;

import java.io.File;
import java.util.Arrays;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantRecipe;
import org.bukkit.plugin.java.JavaPlugin;

import net.islandearth.dregora.trade.Merchants;
import net.islandearth.dregora.trade.api.MerchantsAPI;

/**
 * A donator merchant, where glow can be toggled and can have 5 trade slots.
 */
@Merchant(name = "donator", description = "A donator merchant, where glow can be toggled and can have 5 trade slots.", permission = "Merchant.donator")
public class DonatorMerchant implements IMerchant {

	private Merchants plugin;
	private Villager merchant;
	private File file;
	private FileConfiguration config;
	
	public DonatorMerchant(Villager merchant) {
		this.plugin = JavaPlugin.getPlugin(Merchants.class);
		this.merchant = merchant;
		this.file = new File(plugin.getDataFolder() + "/data/merchants/" + merchant.getUniqueId() + ".yml");
		if (file.exists()) this.config = YamlConfiguration.loadConfiguration(file);
	}
	
	@Override
	public Villager getMerchant() {
		return merchant;
	}

	@Override
	public OfflinePlayer getOwner() {
		if (!file.exists()) {
			return null;
		}
		
		return Bukkit.getOfflinePlayer(UUID.fromString(config.getString("Owner")));
	}
	
	@Override
	public void setMerchantTrade(Player player, int index, ItemStack... itemStacks) {
    	MerchantRecipe recipe = null;
    	if (itemStacks == null || itemStacks.length == 0) {
    		MerchantRecipe mr = new MerchantRecipe(new ItemStack(Material.AIR), 0);
    		mr.setIngredients(Arrays.asList(new ItemStack(Material.AIR), new ItemStack(Material.AIR)));
    		recipe = mr;
    	} else {
    		MerchantRecipe mr = new MerchantRecipe(itemStacks[2], 0);
    		mr.setIngredients(Arrays.asList(itemStacks[0], itemStacks[1]));
    		mr.setExperienceReward(getConfig().getBoolean("Give Experience"));
    		mr.setVillagerExperience(mr.getVillagerExperience() + getConfig().getInt("Experience Per Trade"));
    		recipe = mr;
    	}
    	merchant.setRecipe(index, recipe);
	}

	@Override
	public FileConfiguration getConfig() {
		return MerchantsAPI.getPlugin().getMerchantFactory().getByName("donator").getConfig();
	}

	@Override
	public int getBaseLevel() {
		return 3;
	}
}
