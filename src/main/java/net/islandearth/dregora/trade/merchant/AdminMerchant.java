package net.islandearth.dregora.trade.merchant;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantRecipe;

import net.islandearth.dregora.trade.api.MerchantsAPI;

/**
 * An admin merchant, where no stock is needed.
 */
@Merchant(name = "admin", description = "An admin merchant, where no stock is needed.", permission = "Merchant.admin")
public class AdminMerchant implements IMerchant {

	private Villager merchant;
	
	public AdminMerchant(Villager merchant) {
		this.merchant = merchant;
	}
	
	@Override
	public Villager getMerchant() {
		return merchant;
	}
	
	@Override
	public void setMerchantTrade(Player player, int index, ItemStack... itemStacks) {
    	MerchantRecipe recipe = null;
    	if (itemStacks == null || itemStacks.length == 0) {
    		MerchantRecipe mr = new MerchantRecipe(new ItemStack(Material.AIR), 0);
    		mr.setIngredients(Arrays.asList(new ItemStack(Material.AIR), new ItemStack(Material.AIR)));
    		recipe = mr;
    	} else {
    		MerchantRecipe mr = new MerchantRecipe(itemStacks[2], 0);
    		mr.setIngredients(Arrays.asList(itemStacks[0], itemStacks[1]));
    		mr.setExperienceReward(getConfig().getBoolean("Give Experience"));
    		mr.setMaxUses(Integer.MAX_VALUE);
    		recipe = mr;
    	}
    	merchant.setRecipe(index, recipe);
	}
	
	@Override
	public OfflinePlayer getOwner() {
		return null;
	}
	
	@Override
	public FileConfiguration getConfig() {
		return MerchantsAPI.getPlugin().getMerchantFactory().getByName("admin").getConfig();
	}

	@Override
	public int getBaseLevel() {
		return 5;
	}
}
