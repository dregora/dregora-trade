package net.islandearth.dregora.trade.merchant;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Villager;

import net.islandearth.dregora.trade.api.MerchantsAPI;

/**
 * Class for interfacing with classes implementing {@link IMerchant} and the {@link Merchant} annotation.
 */
public class MerchantInfo {
	
	private Class<? extends IMerchant> merchant;
	private FileConfiguration config;
	private File file;
	private Merchant annotation;
	
	/**
	 * @param merchant class implementing {@link IMerchant}. The class must have the annotation {@link Merchant}.
	 */
	public MerchantInfo(Class<? extends IMerchant> merchant) {
		this.merchant = merchant;
		this.annotation = merchant.getDeclaredAnnotation(Merchant.class);
		this.file = new File(MerchantsAPI.getPlugin().getDataFolder() + "/merchants/" + getMerchantName() + ".yml");
		this.config = YamlConfiguration.loadConfiguration(file);
	}
	
	/**
	 * @return class of merchant
	 */
	public Class<? extends IMerchant> getMerchant() {
		return merchant;
	}
	
	/**
	 * @return merchants unique name
	 */
	public String getMerchantName() {
		return annotation.name();
	}
	
	/**
	 * @return description of merchant
	 */
	public String getDescription() {
		return annotation.description();
	}
	
	/**
	 * Get the permission required to create this type of merchant
	 * @return permission required to create this type of merchant
	 */
	public String getPermission() {
		return annotation.permission();
	}
	
	/**
	 * @param villager
	 * @return creates a new merchant of the appropiate type from the villager
	 */
	public IMerchant newMerchant(Villager villager) {
		try {
			Constructor<? extends IMerchant> constructor = merchant.getConstructor(Villager.class);
			return constructor.newInstance(villager);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @return configuration of merchant
	 */
	public FileConfiguration getConfig() {
		return config;
	}
	
	/**
	 * Saves the configuration.
	 */
	public void saveConfig() {
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Loads the configuration.
	 */
	public void loadConfig() {
		this.config = YamlConfiguration.loadConfiguration(file);
	}
}
