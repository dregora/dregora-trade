package net.islandearth.dregora.trade.merchant;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import net.islandearth.dregora.trade.api.MerchantsAPI;

/**
 * Class containing {@link MerchantInfo} classes.
 */
public class MerchantFactory {
	
	private Map<String, MerchantInfo> merchants = new HashMap<>();
	
	/**
	 * Creates a new MerchantFactory, automatically registering default merchant classes.
	 */
	public MerchantFactory() {
		registerMerchant(new MerchantInfo(NormalMerchant.class));
		registerMerchant(new MerchantInfo(DonatorMerchant.class));
		registerMerchant(new MerchantInfo(AdminMerchant.class));
		registerMerchant(new MerchantInfo(HireMerchant.class));
	}
	
	/**
	 * @param name name declared by annotation in the merchant class
	 * @return {@link MerchantInfo} representing the specified name
	 */
	public MerchantInfo getByName(String name) {
		return merchants.get(name);
	}
	
	/**
	 * Registers a new merchant type.
	 * @param merchant
	 * @see MerchantInfo
	 * @throws IllegalArgumentException when no annotation has been declared, or a merchant by the name is already registered
	 */
	public void registerMerchant(MerchantInfo merchant) {
		if (merchant.getMerchantName() == null) {
			throw new IllegalArgumentException(merchant.getMerchant() + ": no merchant annotation declared");
		}
		
		if (merchants.containsKey(merchant.getMerchantName())) {
			throw new IllegalArgumentException(merchant.getMerchant() + ": merchant by this name already registered");
		}
		
		try {
			File file = new File(MerchantsAPI.getPlugin().getDataFolder() + "/merchants/" + merchant.getMerchantName() + ".yml");
			if (!file.exists()) {
				file.createNewFile();
			}
			
			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			config.options().copyDefaults(true);
			config.addDefault("Give Experience", true);
			config.addDefault("Max Stock", 12);
			config.addDefault("Experience Per Trade", 20);
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		merchant.loadConfig();
		merchants.put(merchant.getMerchantName(), merchant);
	}
	
	/**
	 * @return collection of all registered {@link MerchantInfo}
	 */
	public Collection<MerchantInfo> getMerchants() {
		return merchants.values();
	}
}
