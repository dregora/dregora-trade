package net.islandearth.dregora.trade.merchant;

import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;

public interface IMerchant {
	
	/**
	 * @return bukkit villager involved
	 */
	public Villager getMerchant();
	
	/**
	 * @return the owner of this merchant, or null if none
	 */
	public OfflinePlayer getOwner();
	
	/**
	 * Sets a merchants trade at the specified index.
	 * @param player player changing trade
	 * @param items items in trade
	 */
	public void setMerchantTrade(Player player, int index, ItemStack... itemStacks);
	
	/**
	 * @return configuration file of merchant type
	 */
	public FileConfiguration getConfig();
	
	public int getBaseLevel();

}
