package net.islandearth.dregora.trade.tasks;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import lombok.AllArgsConstructor;
import net.islandearth.dregora.trade.Merchants;
import net.islandearth.dregora.trade.merchant.HireMerchant;
import net.islandearth.dregora.trade.merchant.IMerchant;

@AllArgsConstructor
public class ExpirationTask implements Runnable {

	private Merchants plugin;
	
	@Override
	public void run() {
		for (IMerchant merchant : plugin.getTradeCache().getMerchants().values()) {
			if (merchant instanceof HireMerchant) {
				HireMerchant hireMerchant = (HireMerchant) merchant;
				if (merchant.getOwner() != null) {
					Player player = Bukkit.getPlayer(merchant.getOwner().getUniqueId());
					if (player != null) {
						LocalDateTime time = LocalDateTime.parse((hireMerchant).getMerchantConfig().getString("expiration"), DateTimeFormatter.ISO_DATE_TIME);
						if (time.isBefore(LocalDateTime.now())) {
							player.sendMessage(plugin.getTranslator().getTranslationFor(player, "expired"));
							File file = new File(plugin.getDataFolder() + "/data/merchants/" + merchant.getMerchant().getUniqueId() + ".yml");
							hireMerchant.getMerchantConfig().set("owner", "none");
							hireMerchant.saveMerchantConfig();
							try {
								merchant.getConfig().save(file);
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
	}
}
