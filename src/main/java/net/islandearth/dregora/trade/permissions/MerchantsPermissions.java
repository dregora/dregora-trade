package net.islandearth.dregora.trade.permissions;

import org.bukkit.entity.Player;

import lombok.Getter;

public enum MerchantsPermissions {
	RENAME("Merchant.rename");
	
	@Getter
	private String permission;
	
	private MerchantsPermissions(String permission) {
		this.permission = permission;
	}

	public boolean hasPermission(Player player) {
		return player.hasPermission(this.getPermission());
	}
}
