package net.islandearth.dregora.trade.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Location;

import lombok.Getter;
import net.islandearth.dregora.trade.merchant.IMerchant;

public class TradeCache {
	
	@Getter
	private Map<UUID, Location> trading = new HashMap<>();
	
	@Getter
	private Map<UUID, IMerchant> tradingEntities = new HashMap<>();	
	@Getter
	private List<UUID> editing = new ArrayList<>();
	
	@Getter
	private Map<UUID, Map<UUID, String>> bought = new HashMap<>();
	
	@Getter
	private Map<UUID, IMerchant> merchants = new HashMap<>();

}
