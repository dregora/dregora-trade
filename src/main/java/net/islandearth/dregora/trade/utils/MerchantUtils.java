package net.islandearth.dregora.trade.utils;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;

import net.islandearth.dregora.trade.api.MerchantsAPI;

public class MerchantUtils {
    
	public static void handleTrade(Player owner, Villager merchant, int index) {
		owner.playSound(owner.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
		
		ItemStack first = merchant.getRecipe(index).getIngredients().get(0);
		ItemStack second = merchant.getRecipe(index).getIngredients().get(1);
		if (first != null && first.getType() != Material.AIR) {
			for (ItemStack item : owner.getInventory().addItem(first).values()) {
				owner.getWorld().dropItem(owner.getEyeLocation(), item).setVelocity(owner.getEyeLocation().getDirection().multiply(1.5));
			}
		}
		
		if (second != null && second.getType() != Material.AIR) {
			for (ItemStack item : owner.getInventory().addItem(second).values()) {
				owner.getWorld().dropItem(owner.getEyeLocation(), item).setVelocity(owner.getEyeLocation().getDirection().multiply(1.5));
			}
		}
	}
	
	public static String getVersion() {
		return Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
	}
	
	public static boolean isAdmin(Villager merchant) {
		File file = new File(MerchantsAPI.getPlugin().getDataFolder() + "/data/merchants/" + merchant.getUniqueId() + ".yml");
		if (file.exists()) {
			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			return config.getBoolean("admin");
		}
		return false;
	}
	
	public static boolean isHire(Villager merchant) {
		File file = new File(MerchantsAPI.getPlugin().getDataFolder() + "/data/merchants/" + merchant.getUniqueId() + ".yml");
		if (file.exists()) {
			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			return config.getBoolean("hire");
		}
		return false;
	}
	
	/*
	 * FROM ESSENTIALS CODE
	 */
	
	// Calculate amount of EXP needed to level up
	public static int getExpToLevelUp(int level){
		if (level <= 15) {
			return 2*level+7;
		} else if (level <= 30) {
			return 5*level-38;
		} else {
			return 9*level-158;
		}
	}
 
	// Calculate total experience up to a level
	public static int getExpAtLevel(int level) {
		if (level <= 16) {
			return (int) (Math.pow(level,2) + 6*level);
		} else if (level <= 31) {
			return (int) (2.5*Math.pow(level,2) - 40.5*level + 360.0);
		} else {
			return (int) (4.5*Math.pow(level,2) - 162.5*level + 2220.0);
		}
	}
 
	// Calculate player's current EXP amount
	public static int getPlayerExp(Player player) {
		int exp = 0;
		int level = player.getLevel();
		
		// Get the amount of XP in past levels
		exp += getExpAtLevel(level);
		
		// Get amount of XP towards next level
		exp += Math.round(getExpToLevelUp(level) * player.getExp());
		 
		return exp;
    }

	// Give or take EXP
	public static int changePlayerExp(Player player, int exp) {
		// Get player's current exp
		int currentExp = getPlayerExp(player);
		   
		// Reset player's current exp to 0
		player.setExp(0);
		player.setLevel(0);
		   
		// Give the player their exp back, with the difference
		int newExp = currentExp + exp;
		player.giveExp(newExp);
		   
		// Return the player's new exp amount
		return newExp;
	}
}
