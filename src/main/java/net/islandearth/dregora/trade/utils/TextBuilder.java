package net.islandearth.dregora.trade.utils;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class TextBuilder {
	
	private TextComponent text;
	
	public TextBuilder(String text) {
		this.text = new TextComponent(text);
	}
	
	public TextBuilder setCommand(String command) {
		text.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/" + command));
		return this;
	}
	
	public TextBuilder setHover(String hover) {
		text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(hover).create()));
		return this;
	}
	
	public TextComponent build() {
		return text;
	}
}
