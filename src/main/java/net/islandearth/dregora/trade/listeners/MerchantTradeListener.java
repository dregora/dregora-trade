package net.islandearth.dregora.trade.listeners;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import lombok.AllArgsConstructor;
import net.islandearth.dregora.trade.Merchants;
import net.islandearth.dregora.trade.events.MerchantTradeEvent;
import net.islandearth.dregora.trade.merchant.AdminMerchant;
import net.islandearth.dregora.trade.merchant.DonatorMerchant;
import net.islandearth.dregora.trade.merchant.IMerchant;
import net.islandearth.dregora.trade.merchant.NormalMerchant;
import net.islandearth.dregora.trade.utils.MerchantUtils;
import net.islandearth.dregora.trade.utils.TextBuilder;

@AllArgsConstructor
public class MerchantTradeListener implements Listener {
	
	private Merchants plugin;
	
	@EventHandler
	public void onTrade(MerchantTradeEvent mte) {
		Player player = mte.getPlayer();
		Villager villager = mte.getMerchant();
		File file = new File(plugin.getDataFolder() + "/data/merchants/" + villager.getUniqueId() + ".yml");
		if (villager.getRecipeCount() >= plugin.getConfig().getInt("Minimum Trades")
				&& !file.exists()) {
			IMerchant merchant = new NormalMerchant(villager);
			if (player.hasPermission(plugin.getMerchantFactory().getByName("donator").getPermission())) merchant = new DonatorMerchant(villager);
			plugin.getTradeCache().getTrading().put(player.getUniqueId(), player.getLocation());
			plugin.getTradeCache().getTradingEntities().put(player.getUniqueId(), merchant);
			player.sendMessage(" ");
			player.sendMessage(plugin.getTranslator().getTranslationFor(player, "trade_offer"));
			player.sendMessage(ChatColor.GRAY + "" + ChatColor.UNDERLINE + "Do you...");
			player.sendMessage(" ");
			player.spigot().sendMessage(new TextBuilder(ChatColor.GREEN + "> " + plugin.getTranslator().getTranslationFor(player, "accept")).setHover(ChatColor.GREEN + "Click to accept.").setCommand("createtradedeal").build());
			player.spigot().sendMessage(new TextBuilder(ChatColor.RED + "> " + plugin.getTranslator().getTranslationFor(player, "decline")).setHover(ChatColor.RED + "Click to decline.").setCommand("declinetradedeal").build());
			player.sendMessage(" ");
		} else if (file.exists()) {
			IMerchant merchant = plugin.getTradeCache().getMerchants().get(villager.getUniqueId());
			if (merchant instanceof AdminMerchant) {
				return;
			}
			
			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			OfflinePlayer owner = Bukkit.getOfflinePlayer(UUID.fromString(config.getString("owner")));
			if (owner.isOnline()) {
				MerchantUtils.handleTrade(owner.getPlayer(), villager, mte.getIndex());
				owner.getPlayer().sendMessage(plugin.getTranslator().getTranslationFor(owner.getPlayer(), "sold_item").replace("{1}", villager.getRecipe(mte.getIndex()).getResult().getAmount() + "x " + villager.getRecipe(mte.getIndex()).getResult().getType().toString()).replace("{0}", player.getName()));
			} else {
				config.set("bought", (config.getInt("bought") + 1) + ":" + mte.getIndex());
				Map<UUID, String> entityMap = new HashMap<>();
				if (plugin.getTradeCache().getBought().containsKey(owner.getUniqueId())) entityMap = plugin.getTradeCache().getBought().get(player.getUniqueId());
				entityMap.put(villager.getUniqueId(), (config.getInt("bought") + 1) + ":" + mte.getIndex());
				plugin.getTradeCache().getBought().put(owner.getUniqueId(), entityMap);
				try {
					config.save(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
