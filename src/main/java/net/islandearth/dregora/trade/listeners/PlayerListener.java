package net.islandearth.dregora.trade.listeners;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import lombok.AllArgsConstructor;
import net.islandearth.dregora.trade.Merchants;
import net.islandearth.dregora.trade.utils.MerchantUtils;

@AllArgsConstructor
public class PlayerListener implements Listener {
	
	private Merchants plugin;
	
	@EventHandler
	public void onJoin(PlayerJoinEvent pje) {
		Player player = pje.getPlayer();
		File pf = new File(plugin.getDataFolder() + "/players/" + player.getUniqueId() + ".yml");
		if (!pf.exists()) {
			try {
				pf.createNewFile();
				FileConfiguration config = YamlConfiguration.loadConfiguration(pf);
				config.set("Merchants", 0);
				config.save(pf);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if (plugin.getTradeCache().getBought().containsKey(player.getUniqueId())) {
			for (LivingEntity entity : Bukkit.getWorld("world_dregora").getLivingEntities()) {
				if (entity instanceof Villager) {
					if (plugin.getTradeCache().getBought().get(player.getUniqueId()).containsKey(entity.getUniqueId())) {
						Villager merchant = (Villager) entity;
						Map<UUID, String> bought = plugin.getTradeCache().getBought().get(player.getUniqueId());
						MerchantUtils.handleTrade(player, merchant, Integer.valueOf(bought.get(entity.getUniqueId()).split(":")[0]));
						player.sendMessage(plugin.getTranslator().getTranslationFor(player, "sold_item").replace("{1}", merchant.getRecipe(Integer.valueOf(bought.get(entity.getUniqueId()).split(":")[0])).getResult().getAmount() + "x "));
						bought.remove(entity.getUniqueId());
						File file = new File(plugin.getDataFolder() + "/data/merchants/" + entity.getUniqueId());
						FileConfiguration config = YamlConfiguration.loadConfiguration(file);
						config.set("bought", "0:0");
						try {
							config.save(file);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
}
