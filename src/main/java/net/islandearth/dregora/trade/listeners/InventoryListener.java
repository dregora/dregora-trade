package net.islandearth.dregora.trade.listeners;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantInventory;

import lombok.AllArgsConstructor;
import net.islandearth.dregora.trade.Merchants;
import net.islandearth.dregora.trade.events.MerchantTradeEvent;
import net.islandearth.dregora.trade.merchant.IMerchant;
import net.islandearth.dregora.trade.merchant.MerchantInfo;

@AllArgsConstructor
public class InventoryListener implements Listener {
	
	private Merchants plugin;
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onClick(InventoryClickEvent ice) {
		if (ice.getWhoClicked() instanceof Player) {
			Player player = (Player) ice.getWhoClicked();
			if (ice.getInventory() instanceof MerchantInventory) {
				MerchantInventory mi = (MerchantInventory) ice.getInventory();
				if (mi.getItem(2) != null) {
					if (ice.getSlot() == 2) {
						if (mi.getHolder() instanceof Villager) {
							Villager merchant = (Villager) mi.getHolder();
							if (!plugin.getTradeCache().getEditing().contains(player.getUniqueId())) {
								Bukkit.getPluginManager().callEvent(new MerchantTradeEvent(player, merchant, mi.getSelectedRecipeIndex()));
							}
						}
					}
				} else {
					if (ice.getSlot() == 2
							&& ice.getClickedInventory() instanceof MerchantInventory
							&& mi.getHolder() instanceof Villager) {
						Villager villager = (Villager) mi.getHolder();
						if (ice.getCursor() != null) {
							if (plugin.getTradeCache().getEditing().contains(player.getUniqueId())) {
								ItemStack first = mi.getItem(0);
								ItemStack second = mi.getItem(1);
								if (first == null) first = new ItemStack(Material.AIR);
								if (second == null) second = new ItemStack(Material.AIR);
								IMerchant merchant = plugin.getTradeCache().getMerchants().get(villager.getUniqueId());
								
								merchant.setMerchantTrade(player, mi.getSelectedRecipeIndex(), (ItemStack[]) Arrays.asList(first, second, ice.getCursor()).toArray());
								if (first != null && first.getType() != Material.AIR) player.getInventory().addItem(first);
								if (second != null && second.getType() != Material.AIR) player.getInventory().addItem(second);
								player.getInventory().addItem(ice.getCursor());
								ice.setCursor(null);
								mi.setItem(0, null);
								mi.setItem(1, null);
								player.closeInventory();
								ice.setCancelled(true);
								
								File file = new File(plugin.getDataFolder() + "/data/merchants/" + villager.getUniqueId() + ".yml");
								FileConfiguration config = YamlConfiguration.loadConfiguration(file);
								for (String amount : config.getStringList("amount")) {
									String[] split = amount.split(";");
									int index = Integer.valueOf(split[0]);
									int amnt = Integer.valueOf(split[1]);
									villager.getRecipe(index).setMaxUses(amnt);
								}
							} else {
								ItemStack result = villager.getRecipe(mi.getSelectedRecipeIndex()).getResult();
								ItemStack item = ice.getCursor();
								File file = new File(plugin.getDataFolder() + "/data/merchants/" + villager.getUniqueId() + ".yml");
								FileConfiguration config = YamlConfiguration.loadConfiguration(file);
								if (result != null) {
									if (result.equals(item)) {
										if (item.getAmount() < result.getAmount()) {
											player.sendMessage(plugin.getTranslator().getTranslationFor(player, "not_enough"));
											return;
										}
										
										MerchantInfo check = plugin.getMerchantFactory().getByName("donator");
										int max_stock = plugin.getTradeCache().getMerchants().get(villager.getUniqueId()).getConfig().getInt("Max Stock");
										if (player.hasPermission(check.getPermission())) max_stock = check.getConfig().getInt("Max Stock");
										if (config.getInt("stock") >= max_stock) {
											player.sendMessage(plugin.getTranslator().getTranslationFor(player, "max_stock"));
											return;
										}
										
										int current = 0;
										for (String amnt : config.getStringList("amount")) {
											String[] split = amnt.split(";");
											if (Integer.valueOf(split[0]).equals(mi.getSelectedRecipeIndex())) {
												current = Integer.valueOf(split[1]);
											}
										}
										
										int amount = (item.getAmount() / result.getAmount()) + current;
										villager.getRecipe(mi.getSelectedRecipeIndex()).setUses(0);
										villager.getRecipe(mi.getSelectedRecipeIndex()).setMaxUses(amount);
										List<String> amnt = config.getStringList("amount");
										if (amnt.contains(mi.getSelectedRecipeIndex() + ";" + amount)) amnt.remove(mi.getSelectedRecipeIndex() + ";" + amount);
										else amnt.add(mi.getSelectedRecipeIndex() + ";" + amount);
										//amnt.add(mi.getSelectedRecipeIndex() + ";" + amount);
										config.set("amount", amnt);
										
										/*List<String> stock = config.getStringList("stock");
										if (stock.contains(mi.getSelectedRecipeIndex() + ";" + config.getInt("stock"))) stock.remove(mi.getSelectedRecipeIndex() + ";" + config.getInt("stock"));
										else stock.add(mi.getSelectedRecipeIndex() + ";" + config.getInt("stock"));
										config.set("stock", stock);*/
										ice.setCursor(new ItemStack(Material.AIR));
										try {
											config.save(file);
										} catch (IOException e) {
											e.printStackTrace();
										}
										player.sendMessage(plugin.getTranslator().getTranslationFor(player, "successfully_added"));
										player.closeInventory();
									} else {
										player.sendMessage(plugin.getTranslator().getTranslationFor(player, "incorrect_item"));
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onClose(InventoryCloseEvent ice) {
		if (ice.getPlayer() instanceof Player) {
			Player player = (Player) ice.getPlayer();
			if (plugin.getTradeCache().getEditing().contains(player.getUniqueId())) {
				player.sendMessage(plugin.getTranslator().getTranslationFor(player, "trade_saved"));
				plugin.getTradeCache().getEditing().remove(player.getUniqueId());
			}
		}
	}
}
