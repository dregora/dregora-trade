package net.islandearth.dregora.trade.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.VillagerAcquireTradeEvent;

import lombok.AllArgsConstructor;
import net.islandearth.dregora.trade.Merchants;

@AllArgsConstructor
public class VillagerListener implements Listener {

	private Merchants plugin;
	
	@EventHandler
	public void onAquire(VillagerAcquireTradeEvent vate) {
		if (plugin.getTradeCache().getMerchants().containsKey(vate.getEntity().getUniqueId())) {
			vate.setCancelled(true);
		}
	}
}
