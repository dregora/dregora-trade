package net.islandearth.dregora.trade.listeners;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantRecipe;

import lombok.AllArgsConstructor;
import net.islandearth.dregora.trade.Merchants;
import net.islandearth.dregora.trade.events.MerchantRemoveEvent;
import net.islandearth.dregora.trade.merchant.AdminMerchant;
import net.islandearth.dregora.trade.merchant.DonatorMerchant;
import net.islandearth.dregora.trade.merchant.HireMerchant;
import net.islandearth.dregora.trade.merchant.IMerchant;
import net.islandearth.dregora.trade.merchant.MerchantInfo;
import net.islandearth.dregora.trade.permissions.MerchantsPermissions;
import net.islandearth.dregora.trade.utils.TextBuilder;

@AllArgsConstructor
public class EntityListener implements Listener {
	
	private Merchants plugin;
	
	@EventHandler
	public void onInteract(PlayerInteractEntityEvent piee) {
		if (piee.getHand() == EquipmentSlot.OFF_HAND) return;
		
		Player player = piee.getPlayer();
		if (piee.getRightClicked() instanceof Villager) {
			Villager villager = (Villager) piee.getRightClicked();
			
			// Load merchant file
			File file = new File(plugin.getDataFolder() + "/data/merchants/" + villager.getUniqueId() + ".yml");
			if (file.exists()) {
				
				// Check that merchant is loaded into cache - if not, add it.
				if (!plugin.getTradeCache().getMerchants().containsKey(villager.getUniqueId())) {
					plugin.load(villager);
				}
				
				FileConfiguration config = YamlConfiguration.loadConfiguration(file);
				// Reset recipe sizes
				//TODO 1.14 update
				IMerchant merchant = plugin.getTradeCache().getMerchants().get(villager.getUniqueId());
				if (player.hasPermission(plugin.getMerchantFactory().getByName("donator").getPermission())) merchant = new DonatorMerchant(villager);
				
				if (villager.getRecipes().size() > merchant.getBaseLevel() + merchant.getMerchant().getVillagerLevel() || villager.getRecipes().isEmpty() || villager.getRecipes().size() < merchant.getBaseLevel() + merchant.getMerchant().getVillagerLevel()) {
					List<MerchantRecipe> recipes = new ArrayList<>();
					for (int i = 0; i < merchant.getBaseLevel() + merchant.getMerchant().getVillagerLevel(); i++) {
						MerchantRecipe recipe = null;
						if (i < villager.getRecipes().size()) {
							recipe = villager.getRecipe(i);
						} else {
							MerchantRecipe mr = new MerchantRecipe(new ItemStack(Material.AIR), 0);
							mr.setIngredients(Arrays.asList(new ItemStack(Material.AIR), new ItemStack(Material.AIR)));
							recipe = mr;
						}
						recipes.add(recipe);
					}
					villager.setRecipes(recipes);
				}
				
				// Reset recipe amounts
				for (String amount : config.getStringList("amount")) {
					String[] split = amount.split(";");
					int index = Integer.valueOf(split[0]);
					int amnt = Integer.valueOf(split[1]);
					villager.getRecipe(index).setMaxUses(amnt);
				}
				
				if (plugin.getTradeCache().getMerchants().get(villager.getUniqueId()) instanceof AdminMerchant) {
					if (player.isSneaking() && player.hasPermission(plugin.getMerchantFactory().getByName("admin").getPermission())) {
						plugin.getTradeCache().getEditing().add(player.getUniqueId());
					}
					return;
				}
				
				// Check whether merchant is available for hire
				if (plugin.getTradeCache().getMerchants().get(villager.getUniqueId()) instanceof HireMerchant) {
					if (plugin.getEconomy() != null && plugin.getMerchantFactory().getByName("hire").getConfig().getInt("cost") > 0) {
						if (player.isSneaking()) {
							if (config.getString("owner").equals("none")) {
								if (player.hasPermission(plugin.getMerchantFactory().getByName("hire").getPermission())) {
									player.sendMessage(" ");
									player.sendMessage(plugin.getTranslator().getTranslationFor(player, "hire")
											.replace("{xp}", String.valueOf(plugin.getMerchantFactory().getByName("hire").getConfig().getInt("xpcost")))
											.replace("{cost}", String.valueOf(plugin.getMerchantFactory().getByName("hire").getConfig().getInt("cost"))));
									player.sendMessage(ChatColor.GRAY + "" + ChatColor.UNDERLINE + "Do you...");
									player.sendMessage(" ");
									player.spigot().sendMessage(new TextBuilder(ChatColor.GREEN + "> " + plugin.getTranslator().getTranslationFor(player, "accept")).setCommand("createtradedeal").build());
									player.spigot().sendMessage(new TextBuilder(ChatColor.RED + "> " + plugin.getTranslator().getTranslationFor(player, "decline")).setCommand("declinetradedeal").build());
									player.sendMessage(" ");
									plugin.getTradeCache().getTrading().put(player.getUniqueId(), player.getLocation());
									plugin.getTradeCache().getTradingEntities().put(player.getUniqueId(), new HireMerchant(villager));
									piee.setCancelled(true);
								}
							}
						}
					} else {
						player.sendMessage(ChatColor.RED + "Vault is not installed!");
					}
				}
				
				// Check if player is owner
				if (config.getString("owner").equals(player.getUniqueId().toString())) {
					// Renaming
					if (player.getInventory().getItemInMainHand() != null) {
						if (player.getInventory().getItemInMainHand().getType() == Material.NAME_TAG) {
							if (!MerchantsPermissions.RENAME.hasPermission(player) || !player.getInventory().getItemInMainHand().hasItemMeta()) {
								piee.setCancelled(true);
							} else {
								villager.setCustomNameVisible(true);
								villager.setCustomName(player.getInventory().getItemInMainHand().getItemMeta().getDisplayName());
								if (!plugin.getConfig().getBoolean("Consume Rename")) {
									piee.setCancelled(true);
								}
							}
							return;
						}
					}
					
					// Open editor
					if (player.isSneaking()) {
						for (MerchantRecipe mr : merchant.getMerchant().getRecipes()) {
							mr.setMaxUses(0);
						}
						
						plugin.getTradeCache().getEditing().add(player.getUniqueId());
					}
				} else if (!(merchant instanceof HireMerchant)){
					player.sendMessage(plugin.getTranslator().getTranslationFor(player, "not_owner"));
					piee.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onHit(EntityDamageByEntityEvent edbee) {
		if (edbee.getEntity() instanceof Villager) {
			if (edbee.getDamager() instanceof Player) {
				Player player = (Player) edbee.getDamager();
				Villager villager = (Villager) edbee.getEntity();
				
				// Check that merchant is loaded into cache - if not, add it.
				if (!plugin.getTradeCache().getMerchants().containsKey(villager.getUniqueId())) {
					plugin.load(villager);
				}
			
				File file = new File(plugin.getDataFolder() + "/data/merchants/" + villager.getUniqueId() + ".yml");
				if (file.exists()) {
					FileConfiguration config = YamlConfiguration.loadConfiguration(file);
					
					if (plugin.getTradeCache().getMerchants().get(villager.getUniqueId()) instanceof AdminMerchant) {
						if (player.isSneaking()) player.sendMessage(plugin.getTranslator().getTranslationFor(player, "admin_cannot_stock"));
						edbee.setCancelled(true);
						edbee.setDamage(0.0);
						return;
					}
					
					if (!config.getString("owner").equals(player.getUniqueId().toString())) {
						if (plugin.getConfig().getBoolean("Prevent Merchant Damage")) {
							edbee.setDamage(0);
							edbee.setCancelled(true);
						}
						return;
					}
					
					if (player.isSneaking()) {
						if (player.getInventory().getItemInMainHand() != null) {
							
							if (config.getString("owner").equals(player.getUniqueId().toString())) {
								ItemStack hand = player.getInventory().getItemInMainHand();
								if (hand != null) {
									MerchantInfo check = plugin.getMerchantFactory().getByName("donator");
									if (player.hasPermission(check.getPermission())) {
										if (hand.getType() == Material.valueOf(check.getConfig().getString("Glow Item"))) {
											villager.setGlowing(!villager.isGlowing());
											edbee.setDamage(0);
											edbee.setCancelled(true);
											return;
										}
									} else {
										player.sendMessage(plugin.getTranslator().getTranslationFor(player, "not_donator"));
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onDeath(EntityDeathEvent ede) {
		Entity entity = ede.getEntity();
		if (entity instanceof Villager) {
			if (plugin.getTradeCache().getMerchants().containsKey(entity.getUniqueId())) {
				File file = new File(plugin.getDataFolder() + "/data/merchants/" + entity.getUniqueId() + ".yml");
				file.delete();
				plugin.getTradeCache().getMerchants().remove(entity.getUniqueId());
				Bukkit.getPluginManager().callEvent(new MerchantRemoveEvent((Villager) entity));
			}
		}
	}
}
