package net.islandearth.dregora.trade.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import lombok.AllArgsConstructor;
import net.islandearth.dregora.trade.Merchants;
import net.islandearth.dregora.trade.events.MerchantCreateEvent;
import net.islandearth.dregora.trade.merchant.IMerchant;

@AllArgsConstructor
public class MerchantCreateListener implements Listener {
	
	private Merchants plugin;
	
	@EventHandler
	public void onCreate(MerchantCreateEvent mce) {
		IMerchant merchant = mce.getMerchant();
		plugin.getTradeCache().getMerchants().put(merchant.getMerchant().getUniqueId(), merchant);
		
		merchant.getMerchant().setVillagerLevel(merchant.getBaseLevel());
	}

}
