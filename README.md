**Javadocs**<br>
You can view the javadoc at https://www.islandearth.net/merchant/doc/.

**Compiling**
>  **Note**<br>If you're compiling from source, you can only use Merchants for personal use. To use it in production, please purchase the plugin.

Merchants builds on Java JDK 1.8 and uses Gradle to manage dependencies. Lombok is also used and you will need to install it: http://www.projectlombok.org

**Metrics**<br>
Merchants uses bStats to collect statistics on the usage of the plugin. I would appreciate you not disabling this as it keeps me motivated and helps to improve the plugin.